<?php

namespace Drupal\cache_control_headers\EventSubscriber;

use Drupal\Core\Cache\Context\CacheContextsManager;
use Drupal\Core\Cache\CacheableResponseInterface;
use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\Core\EventSubscriber\FinishResponseSubscriber;
use Drupal\Core\Language\LanguageManagerInterface;
use Drupal\Core\PageCache\RequestPolicyInterface;
use Drupal\Core\PageCache\ResponsePolicyInterface;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpKernel\Event\ResponseEvent;
use Symfony\Component\HttpKernel\KernelEvents;

/**
 * Response subscriber to handle finished responses.
 */
class CacheControlResponseSubscriber extends FinishResponseSubscriber implements EventSubscriberInterface {

  /**
   * A config object for the cache control header configuration.
   *
   * @var \Drupal\Core\Config\Config
   */
  protected $cacheControlConfig;

  /**
   * Constructs a FinishResponseSubscriber object.
   *
   * @param \Drupal\Core\Language\LanguageManagerInterface $language_manager
   *   The language manager object for retrieving the correct language code.
   * @param \Drupal\Core\Config\ConfigFactoryInterface $config_factory
   *   A config factory for retrieving required config objects.
   * @param \Drupal\Core\PageCache\RequestPolicyInterface $request_policy
   *   A policy rule determining the cacheability of a request.
   * @param \Drupal\Core\PageCache\ResponsePolicyInterface $response_policy
   *   A policy rule determining the cacheability of a response.
   * @param \Drupal\Core\Cache\Context\CacheContextsManager $cache_contexts_manager
   *   The cache contexts manager service.
   * @param bool $http_response_debug_cacheability_headers
   *   (optional) Whether to send cacheability headers for debugging purposes.
   */
  public function __construct(LanguageManagerInterface $language_manager, ConfigFactoryInterface $config_factory, RequestPolicyInterface $request_policy, ResponsePolicyInterface $response_policy, CacheContextsManager $cache_contexts_manager, $http_response_debug_cacheability_headers = FALSE) {
    parent::__construct($language_manager, $config_factory, $request_policy, $response_policy, $cache_contexts_manager, $http_response_debug_cacheability_headers);
    $this->cacheControlConfig = $config_factory->get('cache_control_headers.settings');
  }

  /**
   * Sets extra headers on successful responses.
   *
   * @param \Symfony\Component\HttpKernel\Event\ResponseEvent $event
   *   The event to process.
   */
  public function cacheControlOnRespond(ResponseEvent $event) {
    $request = $event->getRequest();
    $response = $event->getResponse();
    if (!$response instanceof CacheableResponseInterface) {
      if (!$this->isCacheControlCustomized($response)) {
        $this->setResponseNotCacheable($response, $request);
      }

      // HTTP/1.0 proxies do not support the Vary header, so prevent any caching
      // by sending an Expires date in the past. HTTP/1.1 clients ignore the
      // Expires header if a Cache-Control: max-age directive is specified (see
      // RFC 2616, section 14.9.3).
      if (!$response->headers->has('Expires')) {
        $this->setExpiresNoCache($response);
      }
      return;
    }

    $is_cacheable = ($this->requestPolicy->check($request) === RequestPolicyInterface::ALLOW) && ($this->responsePolicy->check($response, $request) !== ResponsePolicyInterface::DENY);

    // Add headers necessary to specify whether the response should be cached by
    // proxies and/or the browser.
    if ($is_cacheable && $this->config->get('cache.page.max_age') > 0) {
      if (!$this->isCacheControlCustomized($response)) {
        // Only add the default Cache-Control header if the controller did not
        // specify one on the response.
        $this->setResponseCacheable($response, $request);
      }
    }
    else {
      // If either the policy forbids caching or the sites configuration does
      // not allow to add a max-age directive, then enforce a Cache-Control
      // header declaring the response as not cacheable.
      $this->setResponseNotCacheable($response, $request);
    }
  }

  /**
   * Overrides setResponseCacheable from core FinishResponseSubscriber.
   *
   * @param \Symfony\Component\HttpFoundation\Response $response
   *   A response object.
   * @param \Symfony\Component\HttpFoundation\Request $request
   *   A request object.
   */
  protected function setResponseCacheable(Response $response, Request $request) {
    $max_age_enabled = $this->cacheControlConfig->get('cached_page_max_age');
    $max_age = (isset($max_age_enabled) && $max_age_enabled == 1) ? ', max-age=' . $this->cacheControlConfig->get('max_age') : "";
    $cached_page_headers = $this->cacheControlConfig->get('cached_page_headers') . $max_age;
    $cachedHeaders = $cached_page_headers ?? 'public, max-age=' . $this->config('cache.page.max_age');
    if ($response->headers->has('Cache-Control')) {
      $response->headers->remove('Cache-Control');
    }
    $response->headers->set('Cache-Control', $cachedHeaders);
  }

  /**
   * Overrides setCacheControlNoCache from core FinishResponseSubscriber.
   *
   * @param \Symfony\Component\HttpFoundation\Response $response
   *   A response object.
   */
  protected function setCacheControlNoCache(Response $response) {
    parent::setCacheControlNoCache($response);
    $uncached_page_headers = $this->cacheControlConfig->get('uncached_page_headers');
    $enable_pragma_no_cache = $this->cacheControlConfig->get('enable_pragma_no_cache');
    $uncachedHeaders = $uncached_page_headers ?? 'no-cache, must-revalidate';
    if ($response->headers->has('Cache-Control')) {
      $response->headers->remove('Cache-Control');
    }
    $response->headers->set('Cache-Control', $uncachedHeaders);
    if (isset($enable_pragma_no_cache) && $enable_pragma_no_cache == 1) {
      if ($response->headers->has('Pragma')) {
        $response->headers->remove('Pragma');
      }
      $response->headers->set('Pragma', 'no-cache');
    }
  }

  /**
   * Registers the methods in this class that should be listeners.
   *
   * @return array
   *   An array of event listener definitions.
   */
  public static function getSubscribedEvents(): array {
    $events[KernelEvents::RESPONSE][] = ['cacheControlOnRespond', 16];
    return $events;
  }

}
