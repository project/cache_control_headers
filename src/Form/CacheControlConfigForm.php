<?php

namespace Drupal\cache_control_headers\Form;

use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Form\FormStateInterface;

/**
 * Configure cache control header settings for this site.
 */
class CacheControlConfigForm extends ConfigFormBase {

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'cache_control_headers_settings';
  }

  /**
   * {@inheritdoc}
   */
  protected function getEditableConfigNames() {
    return ['cache_control_headers.settings'];
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $config = $this->config('cache_control_headers.settings');
    $default_max_age = $this->config('system.performance')->get('cache.page.max_age');
    $form['cached_page_headers'] = [
      '#type' => 'textfield',
      '#title' => 'Cached Page Headers',
      '#description' => $this->t('Enter the "Cache-Control" headers for cached response. Drupal default headers will be max-age with public'),
      '#default_value' => $config->get('cached_page_headers') ?? '',
    ];

    $form['cached_page_max_age'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Enable Max age.'),
      '#default_value' => $config->get('cached_page_max_age') ?? 1,
      '#description' => $this->t('Enable Max age for uncached response'),
    ];

    $form['max_age'] = [
      '#type' => 'textfield',
      '#title' => 'Max age',
      '#description' => $this->t('Enter the max-age. Default would be cache.page.max_age'),
      '#default_value' => $config->get('max_age') ?? $default_max_age,
    ];

    $form['uncached_page_headers'] = [
      '#type' => 'textfield',
      '#title' => 'Uncached Page Headers',
      '#description' => $this->t('Enter the "Cache-Control" headers for uncached response (for ex: for authenticated users). Drupal default headers will be no-cache, must-revalidate, private'),
      '#default_value' => $config->get('uncached_page_headers') ?? '',
    ];

    $form['enable_pragma_no_cache'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Enable Pragma no-cache.'),
      '#default_value' => $config->get('enable_pragma_no_cache') ?? 0,
      '#description' => $this->t('Enable Pragma no-cache for backwards compatibility with HTTP/1.0 clients'),
    ];

    return parent::buildForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    $config = $this->config('cache_control_headers.settings');
    $config->set('cached_page_headers', $form_state->getValue('cached_page_headers'))
      ->set('cached_page_max_age', $form_state->getValue('cached_page_max_age'))
      ->set('max_age', $form_state->getValue('max_age'))
      ->set('uncached_page_headers', $form_state->getValue('uncached_page_headers'))
      ->set('enable_pragma_no_cache', $form_state->getValue('enable_pragma_no_cache'))
      ->save();

    parent::submitForm($form, $form_state);

  }

}
