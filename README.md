# Cache Control Headers

The Cache Control Headers module allows site administrators to finely tune Drupal's Cache Control headers, both for cached and uncached pages. Leveraging the core FinishResponseSubscriber service, it seamlessly integrates the configured Cache-Control headers from the module's settings into the rendering process.

For a full description of the module, visit the
[project page](https://www.drupal.org/project/cache_control_headers)

Submit bug reports and feature suggestions, or track changes in the
[issue queue](https://www.drupal.org/project/issues/cache_control_headers)


## Table of contents

- Requirements
- Installation
- Configuration


## Requirements

This module requires no modules outside of Drupal core.


## Installation

Install as you would normally install a contributed Drupal module. For further
information, see
[Installing Drupal Modules](https://www.drupal.org/docs/extending-drupal/installing-drupal-modules).


## Configuration

The configuration page is at `admin/config/cache_control_headers/settings`,
where you can configure the  Cache-Control headers in the settings.
